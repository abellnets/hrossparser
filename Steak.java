package hrossparser;

import javax.xml.parsers.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;
import java.util.*;
import java.io.*;

/**
 * Good Day. This is the Cool Beans that make up the parser. Part of the
 * HrossParser (stadesys.org).
 */
public class Steak extends DefaultHandler {

    private String[] sent;
    private String[] orgString;
    private Hashtable tags;
//        output some_out = new output();

    public void startDocument() throws SAXException {
        tags = new Hashtable();
    }

    public void endDocument() throws SAXException {
        Enumeration e = tags.keys();
        while (e.hasMoreElements()) {
            String tag = (String) e.nextElement();
            int count = ((Integer) tags.get(tag)).intValue();
            System.out.println("Local Name \"" + tag + "\" occurs " + count + " times");
        }
    }

    public void stIn(String inp) {
        sent = inp.split(" ");
        orgString = sent;
//        some_out.setString(sent);
        String[] sentsPOS = new String[sent.length];
        System.out.println(java.util.Arrays.toString(sent));
        for (int i = 0; i < sent.length; i++) {
            System.out.println("The first letter of index " + i + " is " + Character.toUpperCase(sent[i].charAt(0)) + "!");
            PoS(sent[i]);
            System.out.println("The word " + sent[i] + " is a " /* PoS(sent[i] */);
//            sentsPOS[i] = PoS(sent[i]);
        }
        System.out.println("");
        System.out.println("Therefore, the current data looks like:");
        for (int i = 0; i < sent.length; i++) {
            System.out.println(sent[i] + " = " + sentsPOS[i]);
        }
        System.out.println("");
        System.out.println("Ready.");
    }

    public void PoS(String word) {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setNamespaceAware(true);
        spf.setValidating(true);
        
        String lib;
        lib = ("./src/hrossparser/xml_files/CIDE." + Character.toUpperCase(word.charAt(0)));
        System.out.println("Lib: " + lib);
        try{
            SAXParser saxParser = spf.newSAXParser();
            XMLReader xmlReader = saxParser.getXMLReader();
            xmlReader.setContentHandler(new Steak());
            xmlReader.parse("./src/hrossparser/xml_files/gcide.xml");
            System.out.println("In between dicts.");
            xmlReader.parse(lib);
            xmlReader.setErrorHandler(new MyErrorHandler(System.err));
        } catch(ParserConfigurationException | SAXException | IOException ex) {
            java.util.logging.Logger.getLogger(Steak.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            System.out.println("Continuing \"Happily\"");
        }
        
    }
}

class MyErrorHandler implements ErrorHandler { //Got this from http://docs.oracle.com/javase/tutorial/jaxp/sax/parsing.html and decided to use it because I'm awfully lazy and not great with error handling.
    private PrintStream out;

    MyErrorHandler(PrintStream out) {
        this.out = out;
    }

    private String getParseExceptionInfo(SAXParseException spe) {
        String systemId = spe.getSystemId();

        if (systemId == null) {
            systemId = "null";
        }

        String info = "URI=" + systemId + " Line=" 
            + spe.getLineNumber() + ": " + spe.getMessage();

        return info;
    }

    public void warning(SAXParseException spe) throws SAXException {
        out.println("Alert! " + getParseExceptionInfo(spe));
    }
        
    public void error(SAXParseException spe) throws SAXException {
        String message = "Uh oh! " + getParseExceptionInfo(spe);
        throw new SAXException(message);
    }

    public void fatalError(SAXParseException spe) throws SAXException {
        String message = "Fatality. " + getParseExceptionInfo(spe);
        throw new SAXException(message);
    }
}
