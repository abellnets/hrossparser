package hrossparser;

/**
 * Main method for Adam Ross-Bell's HrossParser code.
 * Calls the GUI window to open.
 */
public class HrossParser {

    public static void main(String[] args) {
        openingWin.starter();
    }
}
