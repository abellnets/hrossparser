package hrossparser;
/**
 * Code-wide defined variables for Adam Ross-Bell's HrossParser code.
 */
public class ETC {
    private final boolean debugMode = true;
    private final float version = 0.3_6026f;
    private final String prog_name = "Hross Parser";
    
    public String returnProgramName(){
        if(debugMode == true){
            return prog_name + " (" /*+ buildInfo() */+ "at version " + version + ") --- DEBUG & DEVEL MODE! ---";
        }
        return prog_name;
    }
    public float returnVersion(){
        return version;
    }
//    javax.swing.Action
    public void openAbout(){
//        System.out.println("OPEN!");
        java.net.URI url;
        try {
            url = new java.net.URI("http://www.stadesys.org");
            java.awt.Desktop.getDesktop().browse(url);
        } catch (java.net.URISyntaxException | java.io.IOException ex) {
            java.util.logging.Logger.getLogger(ETC.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } 
//        return null;
    }
    public void openGCIDE(){
//        System.out.println("OPEN!");
        java.net.URI url;
        try {
            url = new java.net.URI("http://www.ibiblio.org/webster/");
            java.awt.Desktop.getDesktop().browse(url);
        } catch (java.net.URISyntaxException | java.io.IOException ex) {
            java.util.logging.Logger.getLogger(ETC.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } 
//        return null;
    }
    public String salutation(){
        java.util.Date dNow = new java.util.Date();
        java.text.SimpleDateFormat ft = new java.text.SimpleDateFormat ("HH");
        int hour = Integer.parseInt(ft.format(dNow));
        if(hour >= 5 && hour < 10) return "Good Morning!";
        else if(hour >= 10 && hour < 12) return "Good Day.";
        else if(hour >= 12 && hour < 15) return "Good Afternoon.";
        else if(hour >= 15 && hour < 18) return "Good Pr'evening.";
        else if(hour >= 18 && hour < 21) return "Good Evening.";
        else if(hour >= 21 || hour < 5) return "Good Night.";
        else return "Hej!";
    }
    public java.awt.Color warmFuzzyFeeling(){
        java.awt.Color outs;
        java.util.Date dNow = new java.util.Date();
        java.text.SimpleDateFormat ft = new java.text.SimpleDateFormat ("HH");
        int hour = Integer.parseInt(ft.format(dNow));
        if(hour >= 5 && hour < 10) outs = new java.awt.Color(204,193,163);
        else if(hour >= 10 && hour < 12) outs = new java.awt.Color(255,255,255);
        else if(hour >= 12 && hour < 15) outs = new java.awt.Color(242,239,232);
        else if(hour >= 15 && hour < 18) outs = new java.awt.Color(229,224,209);
        else if(hour >= 18 && hour < 21) outs = new java.awt.Color(204,193,163);
        else if(hour >= 21 || hour < 5) outs = new java.awt.Color(60,20,20); // Night is also good with RGB(0,32,40). This is Dark Sienna.
        else outs = new java.awt.Color(200,200,200);
        return outs;
    }
    public java.awt.Color foreWFF(){
        if ((warmFuzzyFeeling().getRed() == 0 && warmFuzzyFeeling().getGreen() == 32 && warmFuzzyFeeling().getBlue() == 40) || (warmFuzzyFeeling().getRed() == 60 && warmFuzzyFeeling().getGreen() == 20 && warmFuzzyFeeling().getBlue() == 20)) return new java.awt.Color(197,219,255);
        else return new java.awt.Color(0,0,0);
    }
    
}
